import React, { useState } from 'react';
import './SearchBar.css';
import MagnifyingGlass from '../../assets/magnifying-glass.svg';
import Cross from '../../assets/cross.svg';

function SearchBar({ onInputSubmit }: any) {
  const [term, setTerm] = useState('');

  /* 
  useEffect(() => {
    
    const timeoutId = setTimeout(() => {

    })
    return () => {
      
    }
  }, [term]) */
  function onInputChange(event: any) {
    setTerm(event.target.value);
    onInputSubmit(term);
  }
  function emptyInput() {
    setTerm('');
  }

  let icon = MagnifyingGlass;
  let selectedFunction = onInputChange;
  if (term === '') {
    icon = MagnifyingGlass;
    selectedFunction = onInputChange;
  } else {
    icon = Cross;
    selectedFunction = emptyInput;
  }

  return (
    <div id="searchbar-container">
      <input
        id="searchbar-input"
        type="text"
        placeholder="Search"
        value={term}
        onChange={onInputChange}
      ></input>
      <img
        id="searchbar-image"
        src={icon}
        alt="magnifying-glass"
        onClick={selectedFunction}
      />
    </div>
  );
}

export default SearchBar;
