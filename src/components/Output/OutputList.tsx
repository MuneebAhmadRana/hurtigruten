import React from 'react';
import Output from './Output';

function OutputList({ ships }: any) {
  const renderedList = ships.map((ship: any) => {
    return <Output ship={ship} key={ship.id}></Output>;
  });

  return <div>{renderedList}</div>;
}

export default OutputList;
