import React from 'react';
import SearchBar from './SearchBar/SearchBar';
import OutputList from './Output/OutputList';
import axios from 'axios';
import { getSearchedShips } from '../api/ships';
class App extends React.Component {
  state = {
    ships: []
  };

  onTermSubmit = async (term: string) => {
    const response = await axios.get(`http://localhost:4000/api/ships/${term}`);
    this.setState({ ships: response.data });
  };

  render() {
    return (
      <div>
        <SearchBar onInputSubmit={this.onTermSubmit}></SearchBar>
        <OutputList ships={this.state.ships}></OutputList>
      </div>
    );
  }
}

export default App;
